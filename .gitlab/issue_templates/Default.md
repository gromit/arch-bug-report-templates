<!--

Please read our Bug reporting guidelines before opening a bug:
https://wiki.archlinux.org/title/Bug_reporting_guidelines

-->

# Description:
<!-- Describe the bug in full detail. -->

# Additional info:

* package version(s):
* config and/or log files:
* link to upstream bug report, if any:

# Steps to reproduce:
<!-- Describe how to reproduce the bug step by step including the commands -->

1. Step 1
2. Step 2
3. ...

<!--

To catch more bugs upfront we would love to have more people in the Arch
Testing Team: https://wiki.archlinux.org/title/Arch_Testing_Team

Also consider the various other ways of getting involved:
https://wiki.archlinux.org/title/Getting_involved

-->
